create database mealPlanner;
use mealPlanner;

create table user(
userId int auto_increment primary key,
userName varchar(30) not null unique,
password varchar(30) not null,
role int,
firstName varchar(30) not null,
lastName varchar(30) not null,
email varchar(30) not null,
sex varchar(10) not null
);
 
create table meal(
mealId int auto_increment primary key,
mealItem varchar(200) not null,
mealType varchar(10) not null,
mealDateTime timestamp default current_timestamp
);

create table comment(
commentId int auto_increment primary key,
mealId int not null,
userId int not null,
userName varchar(30) not null,
comment varchar(200) not null,
commentDateTime timestamp default current_timestamp
);

Insert into meal(mealItem, mealType) values("vat, alu vorta, dal", "Breakfast");
Insert into comment(mealId,userId,userName,comment) values(1,1,"imon","asdasdsa");
