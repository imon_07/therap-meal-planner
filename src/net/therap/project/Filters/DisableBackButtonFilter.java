package net.therap.project.Filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/5/13
 * Time: 2:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class DisableBackButtonFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //System.out.println("doFilter");
        HttpServletResponse hsr = (HttpServletResponse) servletResponse;

        hsr.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        //hsr.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        //hsr.setDateHeader("Expires", 0); // Proxies.
        filterChain.doFilter(servletRequest, hsr);
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
