package net.therap.project.dao;

import net.therap.project.domain.Role;
import net.therap.project.domain.User;
import net.therap.project.util.DatabaseTemplate;
import net.therap.project.util.ObjectRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserDaoImpl implements UserDao {

    @Override
    public void saveUser(User user) {
        DatabaseTemplate.executeInsertQuery("insert into user(userName, password, role,firstName,lastName, email, sex) values( ?, ?,?,?,?,?,?)", user.getUserName(), user.getPassword(), user.getRole(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getSex());
    }

    @Override
    public User getUserByName(String userName) {
        //System.out.println("in getUserByName()");
        String query = "select * from user where userName = '" + userName + "'";
        List<User> list = DatabaseTemplate.queryForObject(query, new ObjectRowMapper<User>() {
            @Override
            public User mapRowObject(ResultSet resultSet) {
                //System.out.println("in mapRowObject()");
                User user = new User();
                try {
                    user.setUserName(resultSet.getString("userName"));
                    user.setPassword(resultSet.getString("password"));
                    user.setFirstName(resultSet.getString("firstName"));
                    user.setLastName(resultSet.getString("lastName"));
                    user.setUserId(resultSet.getInt("userId"));
                    user.setEmail(resultSet.getString("email"));
                    //user.setRole(resultSet.getInt("role"));
                    user.setSex(resultSet.getString("sex"));
                    user.setRole(resultSet.getInt("role"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return user;
            }
        });

        if (list.size() != 0) {
            //System.out.println("list size not empty");
            return list.get(0);
        }
        return null;
    }
}
