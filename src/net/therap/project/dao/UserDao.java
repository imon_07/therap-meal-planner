package net.therap.project.dao;

import net.therap.project.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 6:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {
    void saveUser(User user);

    User getUserByName(String userName);


}
