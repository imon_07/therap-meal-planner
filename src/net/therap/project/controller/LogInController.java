package net.therap.project.controller;

import net.therap.project.domain.Comment;
import net.therap.project.domain.Meal;
import net.therap.project.domain.User;
import net.therap.project.service.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/1/13
 * Time: 10:45 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/login.html")
public class LogInController extends HttpServlet {

    private UserService userService;
    private MealService mealService;
    private CommentService commentService;

    public LogInController() {
        userService = new UserServiceImpl();
        mealService = new MealServiceImpl();
        commentService = new CommentServiceImpl();

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("in get");
        HttpSession session = req.getSession();
        if (session.getAttribute("authenticatedUser") != null) {
            //System.out.println("redirecting to userhome");
            resp.sendRedirect("/userhome.html");
        } else {
            RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
            rd.forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("in do post");
        User user = new User();
        user.setUserName(req.getParameter("userName"));
        user.setPassword(req.getParameter("password"));

        User authenticateUser = userService.verifyUser(user);

        if (authenticateUser != null) {
            HttpSession session = req.getSession();
            session.setAttribute("authenticatedUser", authenticateUser);

//            List<Meal> mealList = mealService.getMealList();
//            List<Comment> commentList = commentService.getCommentList();
//            session.setAttribute("mealList",mealList);
//            session.setAttribute("commentList", commentList);
            resp.sendRedirect("/userhome.html");
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
            requestDispatcher.forward(req, resp);
        }

    }
}
