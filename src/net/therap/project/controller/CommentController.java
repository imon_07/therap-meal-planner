package net.therap.project.controller;

import net.therap.project.domain.Comment;
import net.therap.project.domain.User;
import net.therap.project.service.CommentService;
import net.therap.project.service.CommentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/6/13
 * Time: 12:21 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/comment.html")
public class CommentController extends HttpServlet {

    private CommentService commentService;

    public CommentController() {
        commentService = new CommentServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Comment comment = new Comment();
            comment.setComment(req.getParameter("comment"));
            comment.setMealId(Integer.parseInt(req.getParameter("mealId")));
            comment.setUserId(Integer.parseInt(req.getParameter("userId")));
            comment.setUserName(req.getParameter("userName"));

            commentService.saveComment(comment);

            resp.sendRedirect("/userhome.html");

        } else {
            resp.sendRedirect("/login.html");
        }


        //comment.set
    }
}
