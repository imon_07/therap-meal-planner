package net.therap.project.controller;

import net.therap.project.domain.Role;
import net.therap.project.domain.User;
import net.therap.project.service.UserService;
import net.therap.project.service.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/1/13
 * Time: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/registration.html")
public class RegistrationController extends HttpServlet {

    private UserService userService;

    public RegistrationController() {
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/views/registration.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User registerUser = new User();
        registerUser.setUserName(req.getParameter("userName"));
        if (req.getParameter("password").equals(req.getParameter("confirmPassword"))) {
            registerUser.setPassword(req.getParameter("password"));
        }
        registerUser.setFirstName(req.getParameter("firstName"));
        registerUser.setLastName(req.getParameter("lastName"));
        registerUser.setEmail(req.getParameter("email"));
        registerUser.setSex(req.getParameter("sex"));
        registerUser.setRole(2);

        userService.saveUser(registerUser);
        resp.sendRedirect("/login.html");
    }
}
