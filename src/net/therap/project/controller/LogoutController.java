package net.therap.project.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/4/13
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/logout.html")
public class LogoutController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        //System.out.println(session.getId());
        if (session != null) {
            session.invalidate();
        }
        resp.sendRedirect("/login.html");
    }
}
