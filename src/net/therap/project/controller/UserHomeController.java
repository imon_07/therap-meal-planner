package net.therap.project.controller;

import net.therap.project.domain.Comment;
import net.therap.project.domain.Meal;
import net.therap.project.service.CommentService;
import net.therap.project.service.CommentServiceImpl;
import net.therap.project.service.MealService;
import net.therap.project.service.MealServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 10:10 PM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet("/userhome.html")
public class UserHomeController extends HttpServlet {
    private MealService mealService;
    private CommentService commentService;

    public UserHomeController() {
        this.mealService = new MealServiceImpl();
        this.commentService = new CommentServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         //new code
        HttpSession session = req.getSession(false);
        List<Meal> mealList = mealService.getMealList();
        List<Comment> commentList = commentService.getCommentList();
        session.setAttribute("mealList",mealList);
        session.setAttribute("commentList", commentList);
        //new code ends
        RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/userhome.jsp");
        rd.forward(req, resp);
        /*
            This work is done by filter now. AuthorizationFilter do the same thing
        */

        /*
            HttpSession session = req.getSession(false);
            System.out.println(session.getId());
            if(session.getAttribute("authenticatedUser")!=null){
                System.out.println("session not null");
                RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/userhome.jsp");
                rd.forward(req, resp);
            }
            else {
                resp.sendRedirect("/login.html");
            }
        */

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Meal meal = new Meal();
        meal.setMealItem(req.getParameter("mealItem"));
        meal.setMealType(req.getParameter("mealType"));

        mealService.saveMeal(meal);
//        RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/views/userhome.jsp") ;
//        rd.forward(req, resp);
        resp.sendRedirect("/userhome.html");

    }
}
