package net.therap.project.domain;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Role {
    ADMIN(1), USER(2);
    private int value;

    private Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
