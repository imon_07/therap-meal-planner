package net.therap.project.domain;

import java.security.PrivateKey;
import java.sql.Date;
import java.sql.Time;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/1/13
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class Meal {
    private int mealId;
    private String mealItem;
    private String mealType;
    private String mealDateTime;

    public int getMealId() {
        return mealId;
    }

    public String getMealDateTime() {
        return mealDateTime;
    }

    public void setMealDateTime(String mealDateTime) {
        this.mealDateTime = mealDateTime;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;

    }

    public String getMealItem() {
        return mealItem;
    }

    public void setMealItem(String mealItem) {
        this.mealItem = mealItem;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

}
