package net.therap.project.test;

import net.therap.project.util.DatabaseConnection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 3:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestDatabaseConnection {
    public static void main(String[] args) throws SQLException {
//        DatabaseConnection dbConnection = new DatabaseConnection();

        Connection con = DatabaseConnection.getConnection();
        if (con != null) {
            System.out.println(con);
        } else {
            System.out.println("null");
        }
    }
}
