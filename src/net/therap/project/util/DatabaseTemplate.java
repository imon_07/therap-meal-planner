package net.therap.project.util;

import net.therap.project.domain.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseTemplate {

    public static <E> List<E> queryForObject(String query, ObjectRowMapper<E> objectRowMapper){
        //System.out.println("in queryForObject()");
        Connection con = null;
        Statement stmt = null;
        List<E> listOfObject = new ArrayList<E>();
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            resultSet.beforeFirst();
            while(resultSet.next()){
                listOfObject.add(objectRowMapper.mapRowObject(resultSet));
                //System.out.println(listOfObject.get(0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfObject;
    }

    public static void executeInsertQuery(String query, Object... objects) {
        Connection con = null;
        PreparedStatement preparedStatement = null;

        try {
            con = DatabaseConnection.getConnection();
            preparedStatement = con.prepareStatement(query);
            int i = 1;
            for (Object object : objects) {
                if (object instanceof String) {
                    preparedStatement.setString(i, (String) object);
                } else if (object instanceof Integer) {
                    preparedStatement.setInt(i, (Integer) object);
                } else if (object instanceof Float) {
                    preparedStatement.setFloat(i, (Float) object);
                } else if (object instanceof Long) {
                    preparedStatement.setLong(i, (Long) object);
                } else if (object instanceof Double) {
                    preparedStatement.setDouble(i, (Double) object);
                } else if (object instanceof Date) {
                    preparedStatement.setDate(i, (Date) object);
                } else if (object instanceof Date) {
                    preparedStatement.setDate(i, (Date) object);
                } else if (object instanceof Time) {
                    preparedStatement.setTime(i, (Time) object);
                }
                i++;
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
