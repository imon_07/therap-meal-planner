package net.therap.project.util;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/3/13
 * Time: 1:22 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ObjectRowMapper<E> {
    E mapRowObject(ResultSet resultSet);
}
