package net.therap.project.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;


/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 2:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseConnection {
    public static DataSource dataSource;
    public static final String URL;
    public static final String DRIVER_NAME;
    public static final String USER_NAME;
    public static final String PASSWORD;


    static {
        final ResourceBundle resourceBundle = ResourceBundle.getBundle("db");
        URL = resourceBundle.getString("jdbc.url.address");
        DRIVER_NAME = resourceBundle.getString("jdbc.driver");
        USER_NAME = resourceBundle.getString("db.user");
        PASSWORD = resourceBundle.getString("db.password");
        dataSource = getDataSource();
    }

    public static Connection getConnection() throws SQLException{
        return dataSource.getConnection();
    }

    private static DataSource getDataSource() {
        try {
            ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();

            comboPooledDataSource.setDriverClass(DRIVER_NAME);
            comboPooledDataSource.setJdbcUrl(URL);
            comboPooledDataSource.setUser(USER_NAME);
            comboPooledDataSource.setPassword(PASSWORD);

            comboPooledDataSource.setMinPoolSize(5);
            comboPooledDataSource.setAcquireIncrement(5);
            comboPooledDataSource.setMaxPoolSize(20);
            return comboPooledDataSource;
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        return null;
    }
}
