package net.therap.project.service;

import net.therap.project.domain.Comment;
import net.therap.project.util.DatabaseTemplate;
import net.therap.project.util.ObjectRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/5/13
 * Time: 10:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommentServiceImpl implements CommentService {
    @Override
    public void saveComment(Comment comment) {
        DatabaseTemplate.executeInsertQuery("Insert into comment(mealId,userId,userName,comment) values( ?, ?, ?, ?)",comment.getMealId(),comment.getUserId(),comment.getUserName(),comment.getComment());
    }

    @Override
    public void deleteComment() {

    }

    @Override
    public void editComment() {

    }

    @Override
    public List<Comment> getCommentList() {
        String query = "select * from comment";
        List<Comment> commentList = DatabaseTemplate.queryForObject(query, new ObjectRowMapper<Comment>() {
            @Override
            public Comment mapRowObject(ResultSet resultSet) {
                Comment comment = new Comment();
                try {
                    comment.setCommentID(resultSet.getInt("commentId"));
                    comment.setMealId(resultSet.getInt("mealId"));
                    comment.setUserId(resultSet.getInt("userId"));
                    comment.setUserName(resultSet.getString("userName"));
                    comment.setCommentDateTime(resultSet.getString("commentDateTime"));
                    comment.setComment(resultSet.getString("comment"));

                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return comment;
            }
        });
        return commentList;
    }
}
