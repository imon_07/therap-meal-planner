package net.therap.project.service;

import net.therap.project.domain.Meal;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/5/13
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MealService {
    public void saveMeal(Meal meal);
    public List<Meal> getMealList();
}
