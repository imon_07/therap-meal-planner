package net.therap.project.service;

import net.therap.project.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 6:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    public void saveUser(User user);

    public User verifyUser(User user);
}
