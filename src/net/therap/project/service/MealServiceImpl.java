package net.therap.project.service;

import net.therap.project.domain.Meal;
import net.therap.project.util.DatabaseTemplate;
import net.therap.project.util.ObjectRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/5/13
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class MealServiceImpl implements MealService {
    @Override
    public void saveMeal(Meal meal) {
        DatabaseTemplate.executeInsertQuery("insert into meal(mealItem, mealType) values(?,?)", meal.getMealItem(), meal.getMealType());
    }

    @Override
    public List<Meal> getMealList() {
        String query = "select * from meal";
        List<Meal> mealList = DatabaseTemplate.queryForObject(query, new ObjectRowMapper<Meal>() {
            @Override
            public Meal mapRowObject(ResultSet resultSet) {
                Meal meal = new Meal();
                try {
                    meal.setMealId(resultSet.getInt("mealID"));
                    meal.setMealType(resultSet.getString("mealType"));
                    meal.setMealItem(resultSet.getString("mealItem"));
                    meal.setMealDateTime(resultSet.getString("mealDateTime"));
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return meal;
            }
        });
        return mealList;
    }
}
