package net.therap.project.service;

import net.therap.project.domain.Comment;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/5/13
 * Time: 10:10 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CommentService {
    public void saveComment(Comment comment);
    public void deleteComment();
    public void editComment();
    public List<Comment> getCommentList();
}
