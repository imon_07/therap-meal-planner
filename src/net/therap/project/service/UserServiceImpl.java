package net.therap.project.service;

import net.therap.project.dao.UserDao;
import net.therap.project.dao.UserDaoImpl;
import net.therap.project.domain.User;
import net.therap.project.util.DatabaseTemplate;

/**
 * Created with IntelliJ IDEA.
 * User: imon
 * Date: 1/2/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    @Override
    public void saveUser(User user) {
        DatabaseTemplate.executeInsertQuery("insert into user(userName, password, role,firstName,lastName, email, sex) values( ?, ?,?,?,?,?,?)", user.getUserName(), user.getPassword(), user.getRole(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getSex());
    }

    @Override
    public User verifyUser(User user) {
        User verifiedUser = userDao.getUserByName(user.getUserName());
        //System.out.println("in verifyUser");
        if (verifiedUser != null && verifiedUser.getPassword().equals(user.getPassword())) {
            //System.out.println("in verifyUser-->in if");
            return verifiedUser;
        }
        return null;
    }
}
