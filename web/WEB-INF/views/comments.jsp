<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: imon
  Date: 5/1/13
  Time: 4:08 AMM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        .textColor{
        color: rgba(0,0,0,0.69);
        }
    </style>
</head>
<body>
<hr width="100%">
<div>
<c:forEach var="meal" items="${mealList}">
<form style="width: 400px; " method="post" action="/comment.html">
    <p class=textColor align="left"><b>${meal.mealType}</b><br>${meal.mealItem}<br>${meal.mealDateTime}</p>

    <p class=textColor align="left">Comments :</p>

    <c:forEach var="comment" items="${commentList}">
        <c:if test="${meal.mealId == comment.mealId}">
            <p class=textColor align="left"><b>${comment.userName}</b><br>${comment.comment}<br>${comment.commentDateTime}</p>
        </c:if>
    </c:forEach>
    <p align="left">write comment :</p>
    <div align="left">
        <input type="hidden" name="mealId" value="${meal.mealId}">
        <input type="hidden" name="userId" value="${authenticatedUser.userId}">
        <input type="hidden" name="userName" value="${authenticatedUser.userName}">
        <textarea name="comment" rows="4" cols="47" style="resize: none; border-color:rgba(5,5,5,0.32) "></textarea>
        <input style="float: right;" type="submit" value="comment">
    </div>

    </c:forEach>
</form>

</div>

</body>
</html>