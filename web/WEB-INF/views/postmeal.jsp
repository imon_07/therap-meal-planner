<%--
  Created by IntelliJ IDEA.
  User: imon
  Date: 1/5/13
  Time: 4:33 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        .TextColor {
            color: rgba(0, 0, 0, 0.69);
        }
    </style>

    <script type="text/javascript">
        function validatePost() {

            var btns = postForm.mealType;
            for (var i = 0; el = btns[i]; i++) {
                if (el.checked) {
                    if (document.postForm.mealItem.value.length > 0) {
                        return true
                    }
                }
            }
            alert("Fields should not be empty");
            return false;
        }
    </script>

    <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>

</head>

<body onload="noBack();"
      onpageshow="if (event.persisted) noBack();" onunload="">
<hr>
<div>
    <form name="postForm" action="/userhome.html" onsubmit="return validatePost()" method="post" style="width: 400px">
        <div align="left">
        <p class="TextColor" align="Left">Meal type :
            <input class="TextColor" type="radio" name="mealType" value="Breakfast">Breakfast
            <input class="TextColor" type="radio" name="mealType" value="Lunch">Lunch
            <input class="TextColor" type="radio" name="mealType" value="Dinner">Dinner
            <input class="TextColor" type="radio" name="mealType" value="Snacks">Snacks
        </p>

        <p class="TextColor" align="left">Meal item :</p>

        <textarea name="mealItem" class="TextColor" rows="5" cols="47"
                  style="resize: none; ;"></textarea>

        <input class="TextColor" type="submit" value="post" style="float: right;">
        </div>
    </form>
</div>
</body>
</html>