<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: imon
  Date: 1/1/13
  Time: 6:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Therap Meal Planner</title>
</head>
<body style="padding-left:300px; padding-right: 300px">
<div align="left" style=" background-color: #6e81ff; height: 100px; padding: 5px">
    <h1 align="center" style="color: #f0f8ff;">Therap Meal Planner</h1>
</div>
<hr>

<%--<div align="left" style="float: left; width: 30%; background-color: rgba(159,214,251,0.62); ">--%>
    <%--<ul>--%>
        <%--<li style="list-style: none; padding: 2px; width: inherit; height: 30px; font-size: 20px; ">--%>
            <%--<a style="text-decoration: none; color: rgba(0,0,0,0.69)" href="/login.html">Today's--%>
                <%--meal</a></li>--%>
        <%--<li style="list-style: none; padding: 2px; width: inherit; height: 30px;font-size: 20px ">--%>
            <%--<a style="text-decoration: none; color: rgba(0,0,0,0.69)" href="/comments.html">Comment</a>--%>
        <%--</li>--%>
    <%--</ul>--%>
<%--</div>--%>
<div align=center style="float: none; width: 100%; background-color: #e5fdff;">

    <div style="height: 50px; padding-top: 10px; padding-right: 10px">
        <div style="float: left">
            <p style="color: rgba(0,0,0,0.69)" align="left">Hi <c:out
                    value="${sessionScope.authenticatedUser.userName}"></c:out>, Welcome to Therap Meal Planner </p>
        </div>
        <div style="float: right; padding: 5px; background-color: rgba(159,214,251,0.62)">
            <a style="text-decoration: none;" href="/logout.html"><b>Log out</b></a>
        </div>
    </div>

    <div>
        <c:if test="${authenticatedUser.role == 1}">
            <jsp:include page="postmeal.jsp"></jsp:include>
        </c:if>
        <jsp:include page="comments.jsp"></jsp:include>
    </div>

</div>

<div align="center" style="background-color: rgba(159,214,251,0.62); height: 50px;">

</div>
</body>
</html>