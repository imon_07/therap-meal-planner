<%--
  Created by IntelliJ IDEA.
  User: imon
  Date: 1/1/13
  Time: 7:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body style="padding-left:300px; padding-right: 300px">
<div align="center" style=" background-color: #6e81ff; height: 100px; padding: 10px">
    <h1 style="color: #f0f8ff;">Therap Meal Planner</h1>
</div>
<hr>
<div align="center" style="background-color: #add8e6; padding: 10px">
    <h1 style="color: rgba(0,0,0,0.69);">Login</h1>
    <form method="post" action="/login.html">
        <table>
            <tr>
                <td style="color: rgba(0,0,0,0.69);">Username :</td>
                <td><input type="text" name="userName"></td>
            </tr>
            <tr>
                <td style="color: rgba(0,0,0,0.69);">Password :</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="Login">
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </form>
    <p style="color: rgba(0,0,0,0.69);">Don't have an account?
        <a style="text-decoration: none;" href="/registration.html">Sign Up</a>
    </p>
</div>
</body>
</html>